@extends('master')
@section('title') Doc :: @parent @stop

@section('content')
    <div class="row">
        <div class="page-header">
            <h2>Documentation Page</h2>
        </div>
    </div>
    <section style="float:left; width:40%;">
        <h1>API Endpoints</h1>

        <h3>Albums</h3>
        <h5>Fetch albums by id</h5>
        <p class="well well-sm">https://api.deezerizr.com/albums/:id</p>
        <h3>Artists</h3>
        <h5>Fetch artists by id</h5>
        <p class="well well-sm">https://api.deezerizr.com/artists/:id</p>
        <h3>Genres</h3>
        <h5>Fetch genres by id</h5>
        <p class="well well-sm">https://api.deezerizr.com/genres/:id</p>
        <h3>Playlists</h3>
        <h5>Fetch playlists by id</h5>
        <p class="well well-sm">https://api.deezerizr.com/playlist/:id</p>
        <h3>Search</h3>
        <h5>Allow you to search something</h5>
        <p class="well well-sm">https://api.deezerizr.com/search?q=eminem</p>
        <h3>Tracks</h3>
        <h5>Fetch tracks by id</h5>
        <p class="well well-sm">https://api.deezerizr.com/tracks/:id</p>
        <h3>Embedded player</h3>
        <h5>Fetch an embedded player for a given track</h5>
        <p class="well well-sm">https://api.deezerizr.com/oembed?url=http://www.deezerizr.com/tracks/:id</p>
    </section>

    <section style="float:right; width:40%;">
        <h1>Errors and Status codes</h1>

        <h3>Found</h3>
        <h5>This is the normal response when everything is going right</h5>
        <p class="well well-sm">302 : found</p>
        <h3>Bad request</h3>
        <h5>Appear when the call to the API is not excepted</h5>
        <p class="well well-sm">400 : bad_request</p>
        <h3>Payment required</h3>
        <h5>Throw when you need to subscribe to a plan to access the response</h5>
        <p class="well well-sm">402 : payment_required</p>
        <h3>Forbidden</h3>
        <h5>If you try to access something you shouldn't!</h5>
        <p class="well well-sm">403 : forbidden</p>
        <h3>Not found</h3>
        <h5>You asked for the moon</h5>
        <p class="well well-sm">404 : not_found</p>
        <h3>Service busy</h3>
        <h5>Have a break</h5>
        <p class="well well-sm">700 : service_busy</p>
    </section>
@endsection
