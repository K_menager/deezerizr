@extends('master')
@section('title') Home :: @parent @stop
@section('content')
    <div class="row">
        <div class="page-header">
            <h2>Home Page</h2>
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="well">
                @if(Auth::user()->subscription != null)
                    <p>You are currently subscribed to {{Auth::user()->plan->name }}</p>
                @else
                    <p>You have subscribed to no plan.</p>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 well">
                <form id="generate" class="form-horizontal" role="form" method="POST" action="{!! URL::to('/generate_api_key') !!}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="col-md-6">
                        <label id="key-label" class="control-label">
                            <p>{{ $apiKey }}</p>
                        </label>
                            </div>
                        <div class="col-md-3">
                            <button type="submit" class="cancel btn btn-primary" style="margin-right: 15px;">
                                Generate my api token
                            </button>
                        </div>

                    </div>
                </form>
                <form id="cancel" class="form-horizontal" role="form" method="POST" action="{!! URL::to('/cancel_api_key') !!}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-sm btn-danger" style="margin-right: 15px;">
                                Destroy
                            </button>
                        </div>
                    </div>
                </form>
            </div>


        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $('#generate').on('submit', function() {
            console.log("sanity check");
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                context: this,
                success: function (data, statut) {
                    console.log(statut);
                    console.log(data);
                    $('#key-label').html(data);
                },
                error: function (resultat, statut, erreur) {
                    console.log(statut);
                    console.log(resultat);
                    console.log(erreur);
                }
            });
            return false;
        });

        $('#cancel').on('submit', function() {
            console.log("sanity check");
            console.log("sanity check");
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                context: this,
                success: function (data, statut) {
                    console.log(statut);
                    console.log(data);
                    $('#key-label').html(data);
                },
                error: function (resultat, statut, erreur) {
                    console.log(statut);
                    console.log(resultat);
                    console.log(erreur);
                }
            });
            return false;
        });

    </script>
@endsection