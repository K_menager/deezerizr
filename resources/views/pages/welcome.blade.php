{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
{{--<title>Laravel</title>--}}

{{--<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>--}}

{{--<style>--}}
{{--html, body {--}}
{{--height: 100%;--}}
{{--}--}}

{{--body {--}}
{{--margin: 0;--}}
{{--padding: 0;--}}
{{--width: 100%;--}}
{{--color: #B0BEC5;--}}
{{--display: table;--}}
{{--font-weight: 100;--}}
{{--font-family: 'Lato';--}}
{{--}--}}

{{--.container {--}}
{{--text-align: center;--}}
{{--display: table-cell;--}}
{{--vertical-align: middle;--}}
{{--}--}}

{{--.content {--}}
{{--text-align: center;--}}
{{--display: inline-block;--}}
{{--}--}}

{{--.title {--}}
{{--font-size: 96px;--}}
{{--margin-bottom: 40px;--}}
{{--}--}}

{{--.quote {--}}
{{--font-size: 24px;--}}
{{--}--}}
{{--</style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="container">--}}
{{--<div class="content">--}}
{{--<div class="title">Laravel 5</div>--}}
{{--<div class="quote">{{ Inspiring::quote() }}</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</body>--}}
{{--</html>--}}
@extends('master')
@section('title') Home :: @parent @stop
@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>This is your new way to fetch music throught Deezer</h1>
            <p>We provide an awesome API allowing you to fetch anything from Deezer, without restriction.</p>
            <p><a class="btn btn-primary btn-lg" href="" role="button">Learn more &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>Easy</h2>
                <p>We provide a powerful and easy to use wrapper, next to the classic Deezer API.</p>
                <p><a class="btn btn-default" href="" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Efficient</h2>
                <p>Just try it, you've got the control in few minutes!</p>
                <p><a class="btn btn-default" href="" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Affordable</h2>
                <p>Discover ours awesome princing plans and just forget headaches :)</p>
                <p><a class="btn btn-default" href="" role="button">View details &raquo;</a></p>
            </div>
        </div>

        <hr>

        <footer>
            <p>&copy; Deezerizr 2015</p>
        </footer>
    </div> <!-- /container -->
@endsection