@extends('master')
@section('title') Pricing :: @parent @stop
@section('content')
    <div class="row">
        <div class="page-header">
            <h2>Pricing Page</h2>
        </div>
        <div class="row">
            @if(count($plans)>0)
                @foreach($plans as $plan)
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ $plan->name }}</h3>
                            </div>
                            <div class="panel-body">
                                <p>{{ $plan->description }}</p>
                                <p style="text-align:right; padding-right:15px;">Price : {{ $plan->monthly_price }} $</p>
                            </div>
                            @if (Auth::guest())
                                <form class="form-horizontal" role="form" method="GET" action="{!! URL::to('auth/register') !!}">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary" style="margin-top:-10px;">
                                                Sign In
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                {{--{{ Auth::user()->subscription }}--}}
                                {{--@if (Auth::user()->plan->plan_id  == $plan->plan_id)--}}
                                    {{--<button class="btn btn-secondary">--}}
                                        {{--Current plan--}}
                                    {{--</button>--}}
                            {{--{{ dd(Auth::user()->subscription == null) }}--}}
                                @if(Auth::user()->subscription != null && Auth::user()->subscription->subscribed())
                                    <form action="plans/{{$plan->id}}/subscription/upgrade" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-primary" style="margin-top:-10px;">
                                                    Upgrade
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <form action="plans/{{$plan->id}}/subscription" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <script
                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                data-key="pk_test_BWIFYm7ZIfeFAAIFU97k3nfZ"
                                                data-amount='{{ $plan->monthly_price * 100 }}'
                                                data-name="Deezerizr"
                                                data-email='{{ Auth::user()->email }}',
                                                data-description='{{ $plan->description }}'
                                                data-image="/128x128.png">
                                        </script>
                                    </form>
                                @endif
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-12">
                    <h2>No plans available yet.<br />Coming soon...</h2>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')

@endsection