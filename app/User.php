<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }

    /**
     * @return mixed
     */
    public function plan()
    {
        $relation = $this->hasOne('App\Subscription');
        if ($relation->getResults())
            return $relation->getResults()->plan();
        else
            return $relation;
    }

    public function subscribed()
    {
        return $this->subscription()->getResults() == null || $this->subscription()->getResults()->subscribed();
    }

    public function hasStripeId()
    {
        return $this->subscription()->getResults()->hasStripeId();
    }
}
