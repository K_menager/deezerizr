<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller {
    public function welcome()
    {
        return view('pages.welcome');
    }
    public function documentation()
    {
        return view('pages.documentation');
    }
    public function plans()
    {
        return view('pages.plans');
    }
}