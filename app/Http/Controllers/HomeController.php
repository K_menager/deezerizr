<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: kiyoakimenager
 * Date: 16/06/15
 * Time: 14:19
 */

use Chrisbjr\ApiGuard\Models\ApiKey;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('auth');
        //parent::__construct();
        //$this->news = $news;
        //$this->user = $user;
    }
    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        $apiKey = ApiKey::where('user_id', '=', $user->id)->first()->key;

        if (!isset($apiKey))
            $apiKey = 'No api token generated';

        return view('pages.home')->with('apiKey', $apiKey);
    }
}