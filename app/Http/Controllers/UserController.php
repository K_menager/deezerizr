<?php

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Models\ApiKey;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * @return mixed
     */
    public function generate_api_key()
    {
        $user = Auth::user();

        $apiKey = ApiKey::where('user_id', '=', $user->id)->first();
//        $apiKey->delete();

        if (!isset($apiKey)) {
            $apiKey                = new ApiKey;
            $apiKey->user_id       = $user->id;
            $apiKey->key           = $apiKey->generateKey();
            $apiKey->level         = 1; // Should be User->subscription->level
            $apiKey->ignore_limits = 0;
        } else {
            $apiKey->generateKey();
        }

        if (!$apiKey->save()) {
            return $this->response->errorInternalError("Failed to create an API key. Please try again.");
        }

        return view('pages.key_label')->with('key', $apiKey->key);
    }

    /**
     * @return mixed
     */
    public function cancel_api_key()
    {
        $user = Auth::user();

        $apiKey = ApiKey::where('user_id', '=', $user->id)->first();

        if (!isset($apiKey)) {
            $apiKey->delete();
        }

        return view('pages.key_label')->with('key', 'Key destroyed');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
