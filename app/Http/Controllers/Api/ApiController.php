<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Request as R;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;

class ApiController extends ApiGuardController
{
    protected $client;
    protected $apiMethods;

    public function __construct()
    {
        $this->apiMethods = [
            'all' => [
                'keyAuthentication' => true,
                'level' => 0,
                'limits' => [
                    // The variable below sets API key limits
                    'key' => [
                        'increment' => '1 hour',
                        'limit' => 3
                    ],
                    // The variable below sets API method limits
                    'method' => [
                        'increment' => '1 day',
                        'limit' => 3
                    ]
                ]
            ]
        ];
        $this->client = new \GuzzleHttp\Client(['base_uri' => 'https://api.deezer.com/']);
        parent::__construct();
    }

    /**
     * https://api.deezerizr.com/search?q=eminem
     *
     * @return Response
     */
    public function search()
    {
        $url= R::all();
        $q = $url['q'];

        //Here an example of how to catch an exception. Let's catch deezer's api error's this way ?
        try {
            $uri = 'search?q=' . $q;
            $res = $this->client->get($uri);

            // Let's catch deezer's api error's testing the status code ?
            $status_code = $res->getStatusCode();

            $response = $res->getBody();
            return $response;

        } catch (ModelNotFoundException $e) {

            // Use api-response formatter to respond in well formartted way the error.
            return $this->response->errorNotFound();

        }
    }

    /**
     * https://api.deezerizr.com/oembed?url=http://www.deezerizr.com/tracks/:id
     *
     * @return Response
     */
    public function oembed()
    {
        // Here the url to oembed
        $url= R::all();
        $url_oembed = $url['url'];


        try {
            $uri = 'oembed?url=' . $url_oembed;
            $res = $this->client->get($uri);

            // Let's catch deezer's api error's testing the status code ?
            $status_code = $res->getStatusCode();

            $response = $res->getBody();
            return $response;

        } catch (ModelNotFoundException $e) {

            // Use api-response formatter to respond in well formartted way the error.
            return $this->response->errorNotFound();

        }
    }
}
