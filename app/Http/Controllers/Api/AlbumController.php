<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;

class AlbumController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try {

            // Here, first cache should be performed
            // formulate the request to deezer api
            $uri = 'album/' . $id;
            $res = $this->client->get($uri);

            // Let's catch deezer's api error's testing the status code ?
            $status_code = $res->getStatusCode();

            $response = $res->getBody();
            return $response;

            // $formattedResponse = $this->response->withItem($response, new AlbumTransformer());
            // user api-response formatter to respond in well formartted way.
            // return $formattedResponse;

        } catch (ModelNotFoundException $e) {

            // Use api-response formatter to respond in well formartted way the error.
            return $this->response->errorNotFound();

        }
    }
}
