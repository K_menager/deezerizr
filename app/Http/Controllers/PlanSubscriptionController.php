<?php

namespace App\Http\Controllers;

use App\User;
use App\Plan;
use App\Subscription;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class PlanSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    protected $rules = array(
        'stripeToken' => ['required']
    );

    /**
     * Store a newly created resource in storage.
     *
     * @param $plan_id
     * @return Response
     */
    public function store($plan_id)
    {

        try {
            $input = Input::all();
            $plan = Plan::findOrFail($plan_id);
            $user = Auth::user();


                $validator = Validator::make($input, $this->rules);

                if ($validator->fails()) {
                    return Redirect::route('plans')->withErrors($validator)->withInput();
                }
                $input['plan_id'] = $plan->id;
                $input['user_id'] = $user->id;


                $token = Input::get('stripeToken');

                $subscription = Subscription::create($input);
                $subscription->subscription($plan->plan_id)->create($token);

        }
        catch (Exception $e) {
            //Error
        }
        $message = 'Congratulation, you subscribed to ' .$plan->name .' plan !';
        return redirect()->action('HomeController@index')->with('message', $message);
    }

    public function upgrade($plan_id)
    {

        try {
            $plan = Plan::findOrFail($plan_id);
            $user = Auth::user();

            $input['plan_id'] = $plan->id;
            $input['user_id'] = $user->id;

            $subscription = $user->subscription;
            $subscription->plan_id = $plan->id;
            $subscription->save();
            $subscription->subscription($plan->plan_id)->swap();

        }
        catch (Exception $e) {
            //Error
        }
        $message = 'Congratulation, your plan has been upgraded to ' .$plan->name .'\'s plan !';
        return redirect()->action('HomeController@index')->with('message', $message);
    }

    public function cancel()
    {
        $subscription = Auth::user()->subscription;
        $subscription->subscription()->cancel();

        $message = 'Subscription canceled...';
        return redirect()->action('HomeController@index')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
