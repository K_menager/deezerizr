<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function() {
    return view('pages.welcome');
});

Route::get('documentation', 'PagesController@documentation');

Route::resource('plans', 'PlansController@index',
    ['only' => ['index']]
);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);



/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => 'auth'], function() {
    Route::get('home', 'HomeController@index');

    Route::post('generate_api_key', 'UserController@generate_api_key');
    Route::post('cancel_api_key', 'UserController@cancel_api_key');

    Route::resource('plans.subscription', 'PlanSubscriptionController',
        ['only' => ['store']]);
    Route::post('plans/{id}/subscription/upgrade', 'PlanSubscriptionController@upgrade');
    Route::post('plans/{id}/subscription/cancel', 'PlanSubscriptionController@cancel');
});



/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here goes all route related to the api
|
*/

Route::get('api/v1/albums/{id}', 'Api\AlbumController@show');
Route::get('api/v1/artists/{id}', 'Api\ArtistController@show');
Route::get('api/v1/genres/{id}', 'Api\GenreController@show');
Route::get('api/v1/playlists/{id}', 'Api\PlaylistController@show');
Route::get('api/v1/tracks/{id}', 'Api\TrackController@show');

Route::get('api/v1/search', 'Api\ApiController@search'); // https://api.deezerizr.com/search?q=eminem
Route::get('api/v1/oembed', 'Api\ApiController@oembed'); // https://api.deezerizr.com/oembed?url=http://www.deezerizr.com/tracks/:id
