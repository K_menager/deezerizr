<?php
/**
 * Created by PhpStorm.
 * User: kiyoakimenager
 * Date: 17/06/15
 * Time: 21:06
 */

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('plans')->delete();

        $plans = array(
            ['id' => 1,
                'description' => 'Enjoy the appetizer',
                'name' => 'EXPLORATOR', 'plan_id' => 'explorator', 'monthly_price' => 0, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2,
                'description' => 'A good trip is waiting for you !',
                'name' => 'NAVIGATOR', 'plan_id' => 'navigator', 'monthly_price' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3,
                'description' => 'You have no idea what you\'ll reach with that !',
                'name' => 'MAXIMATOR', 'plan_id' => 'maximator', 'monthly_price' => 10,'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        // Uncomment the below to run the seeder
        DB::table('plans')->insert($plans);
    }

}