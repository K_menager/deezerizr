<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('plan_id')->unsigned();
            $table->timestamps();

            $table->foreign('plan_id')->references('id')->on('plans'); //->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriptions');
//        Schema::table('subscriptions', function (Blueprint $table) {
//            $table->dropForeign('subscriptions_user_id_foreign');
//            $table->dropForeign('subscriptions_plan_id_foreign');
//        });

    }
}
