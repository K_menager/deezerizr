## Deezerizr

L'api qui tue.

## Set up front-end's dependencies with bower

You must install [node and npm](http://nodejs.org/) then install bower by running:
    
    npm install -g bower

Go in deezrizr's root directory and run :

    bower install

All front-end dependencies should now be installed.
Now in order to compile all the css and stuffs, run :

    gulp

## Homestead

You must install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) as well as [Vagrant](http://www.vmware.com/)

Once done run :

    vagrant box add laravel/homestead

Install composer globally on your local machine (Make sure ~/.composer/vendor/bin is part of your PATH). Then run :

    composer global require "laravel/homestead"

Then create your homestead.yaml file by runing :

    homestead init

And edit homestead.yaml based on the following example :

    homestead edit

Example :

    #filename: "homestead.yaml"
    ---
    ip: "192.168.10.10"
    memory: 2048
    cpus: 1
    provider: virtualbox
    
    authorize: ~/.ssh/id_rsa.pub
    
    keys:
        - ~/.ssh/id_rsa
    
    folders:
        - map: ~/PATH_TO/deezerizr
          to: /home/vagrant/Code/deezerizr
    
    sites:
        - map: deezerizr.dev
          to: /home/vagrant/Code/deezerizr/public
    
    databases:
        - homestead
    
    variables:
        - key: APP_ENV
          value: local

Finally update your hosts file (/etc/hosts) by pasting the new host you just created at the end of it:

    # filename: "/etc/hosts"
    # ...
    # previously added hosts
    # ...

    192.168.10.10   deezerizr.dev # The new host

Boot up your VM by typing :
    
    homestead up

Type deezerizr.dev in your browser to see the app running on your virtual box.

Documentation for homestead installation can be found on the [Laravel website](http://laravel.com/docs/5.1/homestead).
or check the video on [Laracasts website](https://laracasts.com/series/laravel-5-fundamentals/episodes/2).

##Api usage

First connect to an account.

Generate an api key (This api key is user related).

Set the key into X-Authorization header node.

    curl --header "X-Authorization: HERE_GOES_API_KEYS" http://deezerizr.dev/api/v1/albums/1